defmodule TLV8DecoderTest do
  use ExUnit.Case

  @data_pool Enum.reduce(1..512, <<>>, fn(x, acc) -> acc <> <<x::unsigned-big-integer-size(8)>> end)
  @data_fe binary_part(@data_pool, 0, 254)
  @data_ff binary_part(@data_pool, 0, 255)
  @data_ff_2 binary_part(@data_pool, 255, 255)
  @data_100 binary_part(@data_pool, 0, 256)
  @data_1fe binary_part(@data_pool, 0, 510)

  test "decodes a TLV8 with a single item" do
    assert [ %TLV8{type: 0x00, value: <<>>} ] == TLV8.decode(<<0x00, 0x00>>)
    assert [ %TLV8{type: 0x01, value: <<0x02>>} ] == TLV8.decode(<<0x01, 0x01, 0x02>>)
    assert [ %TLV8{type: 0x0E, value: @data_fe } ] == TLV8.decode(<<0x0E, 0xFE>> <> @data_fe)
    assert [ %TLV8{type: 0x0F, value: @data_ff } ] == TLV8.decode(<<0x0F, 0xFF>> <> @data_ff)
  end

  test "decodes a TLV8 with two items" do
    assert [ %TLV8{type: 0x00, value: <<>>}, %TLV8{type: 0x01, value: <<0x02>>} ] == TLV8.decode(<<0x00, 0x00, 0x01, 0x01, 0x02>>)
    assert [ %TLV8{type: 0x00, value: <<0x01>>}, %TLV8{type: 0x02, value: <<0x03, 0x04>>} ] == TLV8.decode(<<0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x04>>)
  end

  test "decodes a TLV8 with multiple fragments" do
    assert [ %TLV8{type: 0xFF, value: @data_100} ] == TLV8.decode(<<0xFF, 0xFF>> <> @data_ff <> <<0xFF, 0x01, 0x00>>)
    assert [ %TLV8{type: 0x01, value: @data_1fe} ] == TLV8.decode(<<0x01, 0xFF>> <> @data_ff <> <<0x01, 0xFF>> <> @data_ff_2)
  end

  test "decodes a TLV8 with multiple fragments and multiple items" do
    assert [ %TLV8{type: 0x11, value: @data_fe }, %TLV8{type: 0x22, value: @data_1fe}, %TLV8{type: 0x33, value: <<0x44>>} ] == TLV8.decode(<<0x11, 0xFE>> <> @data_fe <> <<0x22, 0xFF>> <> @data_ff <> <<0x22, 0xFF>> <> @data_ff_2 <> <<0x33, 0x01, 0x44>>)
  end

  test "decodes a TLV8 with a single invalid item" do
    assert [ :invalid_tlv8 ] == TLV8.decode(<<>>)
    assert [ :invalid_tlv8 ] == TLV8.decode(<<0x01, 0x01>>)
    assert [ :invalid_tlv8 ] == TLV8.decode(<<0x02, 0x02, 0x02>>)
  end

  test "decodes a TLV8 with a valid item followed by an invalid item" do
    assert [ %TLV8{type: 0x00, value: <<>>}, :invalid_tlv8 ] == TLV8.decode(<<0x00, 0x00, 0x00>>)
    assert [ %TLV8{type: 0x01, value: <<0x02>>}, :invalid_tlv8 ] == TLV8.decode(<<0x01, 0x01, 0x02, 0x01, 0x01>>)
    assert [ %TLV8{type: 0x0F, value: @data_fe }, :invalid_tlv8 ] == TLV8.decode(<<0x0F, 0xFE>> <> @data_ff)
  end


end
