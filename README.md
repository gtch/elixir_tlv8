# TLV8

A small library that decodes and encodes TLV8 according to Apple's specifications. It is intended to be compatible
with TLV8 data structures used by HomeKit.

* Encodes a list of type & value pairs into binary
* Decodes binary into a list of type & value pairs
* Automatically handles values longer than 255 bytes by splitting them into fragments

This TLV8 library is written by Charles Gutjahr, but was inspired by [elixir_tlv](https://github.com/bitgamma/elixir_tlv). 
That library implements the  ASN.1 BER-TLV format, a format that is somewhat similar to the Apple TLV8 format 
but is sufficiently different for that library to be unsuitable for working with Apple TLV8.

Licensed under the MIT license, see the LICENSE file for details.
 
  
## Installation
Add tlv8 to your list of dependencies in `mix.exs`:
```elixir
def deps do
  [{:tlv8, "~> 0.1.0"}]
end
```
## Examples

### Decoding TLVs
Use `TLV8.decode` to extract a list of TLV8 items from the given binary data.

```elixir
# Decoding binary data containing a single TLV8
iex> TLV8.decode(<<0xFF, 0x02, 0x03, 0x04>>)
[%TLV8{type: 255, value: <<3, 4>>}]

# Decoding binary data containing multiple TLV8s
iex> TLV8.decode(<<0x06, 0x01, 0x03, 0x01, 0x05, 0x68, 0x65, 0x6c, 0x6c, 0x6f>>)
[%TLV8{type: 6, value: <<3>>}, %TLV8{type: 1, value: "hello"}]

# Invalid TLV8
iex> TLV8.decode(<<0x80, 0x02, 0x00>>)
[:invalid_tlv8]
```

### Encoding TLVs
Use `TLV8.encode` to generate TLV8 binary data from either a single TLV8 item or a list of TLV8 items.

```elixir
# Encoding a single TLV8
iex> TLV8.encode(%TLV8{type: 0xFF, value: <<0x03, 0x04>>})
<<0xFF, 0x02, 0x03, 0x04>>

# Encoding multiple TLV8s
iex> TLV8.encode([%TLV8{type: 0x06, value: <<0x03>>}, %TLV8{type: 1, value: "hello"}])
<<0x06, 0x01, 0x03, 0x01, 0x05, 0x68, 0x65, 0x6c, 0x6c, 0x6f>>

# Encoding an invalid TLV8 type
iex> TLV8.encode([%TLV8{type: 0x00, value: <<>>}, %TLV8{type: 0x100, value: <<0x00>>}])
:invalid_tlv8
```