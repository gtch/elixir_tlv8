defmodule Tlv8.Mixfile do
  use Mix.Project

  def project do
    [app: :tlv8,
     version: "0.1.0",
     elixir: "~> 1.2",
     description: "Encodes and decodes Apple TLV8 structures",
     package: [
       maintainers: ["Charles Gutjahr"],
       licenses: ["MIT"],
       links: %{"Bitbucket" => "https://bitbucket.org/gtch/elixir_tlv8"}
     ],
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: [
       {:ex_doc, "~> 0.10", only: :dev},
       {:earmark, "~> 0.1", only: :dev},
     ],
     docs: [
       extras: ["README.md"]
     ]
    ]
  end

  def application do
    [applications: [:logger]]
  end

end
