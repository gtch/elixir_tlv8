defmodule TLV8Encoder do
  @moduledoc false

  def encode(tlv8s) when is_list(tlv8s) do
    try do
      Enum.reduce(tlv8s, <<>>, fn(tlv8, acc) -> acc <> encode_type_value(tlv8.type, tlv8.value) end)
    catch
      _ -> :invalid_tlv8
    end
  end

  def encode(tlv8) do
    try do
      encode_type_value(tlv8.type, tlv8.value)
    catch
      _ -> :invalid_tlv8
    end
  end


  defp encode_type_value(type, _value) when type < 0 or type > 255 do
    throw(:invalid_tlv8)
  end

  defp encode_type_value(type, value) when byte_size(value) > 255 do
    <<value255::binary-size(255), remainder::binary>> = value
    <<type, 255>> <> value255 <> encode_type_value(type, remainder)
  end

  defp encode_type_value(type, value) do
    <<type, byte_size(value)>> <> value
  end

end